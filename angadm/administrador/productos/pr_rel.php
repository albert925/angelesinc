<?php
	session_start();
	include '../../../config.php';
	if (isset($_SESSION['adm'])) {
		$usrRad=$_SESSION['adm'];
		$datos_adm="SELECT * from administrador where useradm='$usrRad'";
		$sql_datad=$conexion->query($datos_adm) or die (mysqli_error());
		while ($ad=$sql_datad->fetch_assoc()) {
			$idad=$ad['id_adm'];
			$usad=$ad['useradm'];
			$corad=$ad['cor_adm'];
		}
		$idR=$_GET['pd'];
		if ($idR=="") {
			echo "<script type='text/javascript'>";
				echo "alert('Id producto no disponible');";
				echo "var pagina='../productos';";
				echo "document.location.href=pagina;";
			echo "</script>";
		}
		else{
			$datPDs="SELECT * from producto where cod_p='$idR'";
			$sql_PDs=$conexion->query($datPDs) or die (mysqli_error());
			while ($sDP=$sql_PDs->fetch_assoc()) {
				$idPd=$sDP['cod_p'];
				$nmPd=$sDP['nam_p'];
			}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, maximun-scale=1" />
	<title>Angeles Inc</title>
	<link rel="icon" href="../../../imagenes/icono.png" />
	<link rel="stylesheet" href="../../../css/normalize.css" />
	<link rel="stylesheet" href="../../../css/iconos/style.css" />
	<link rel="stylesheet" href="../../../css/style.css" />
	<link rel="stylesheet" href="../../../css/styleadmin.css" />
	<script src="../../../js/jquery_2_1_1.js"></script>
	<script src="../../../js/scripadmin.js"></script>
	<script src="../../../ckeditor/ckeditor.js"></script>
	<script src="../../../js/productos.js"></script>
</head>
<body>
	<header>
		<figure id="logo">
			<a href="../">
				<img src="../../../imagenes/2.png" alt="logo" />
			</a>
		</figure>
	</header>
	<article id="mnuPp">
		<nav id="mnP">
			<ul>
				<li class="submen" data-num="3"><a href="../imagenes">Imágenes</a>
					<ul class="children3">
						<li><a href="../markfoot">Marcas I.</a></li>
					</ul>
				</li>
				<li><a class="selC" href="../productos">Productos</a></li>
				<li class="submen" data-num="1">
					<a href="../videos">Videos</a>
				</li>
				<li><a href="../eventos">Eventos</a></li>
				<li class="submen" data-num="2"><a href="../factura">Historial Ventas</a>
				</li>
				<li><a href="../usuarios">Usuarios</a></li>
				<li><a href="../sectores">Sectores</a></li>
				<li><a href="../donde">Donde Estamos</a></li>
				<li><a href="../../../cerrar">Salir</a></li>
			</ul>
		</nav>
		<div id="mnmov"><span class="icon-menu"></span></div>
	</article>
	<nav id="mnA">
		<a href="../"><?php echo "$usrRad"; ?></a>
		<a href="../productos">Ver Productos</a>
		<a href="tipo_P.php">Tipos de Producto</a>
		<a href="clase_p.php">Colecciones</a>
		<a href="mark_p.php">Marcas de producto</a>
		<a href="#" id="nvD">Rel. Pro-Prec</a>
	</nav>
	<section>
		<h1>Imagenes de <?php echo "$nmPd"; ?></h1>
		<article id="automargen">
			<article id="mD" class="ocultingre">
				<form action="#" method="post" enctype="multipart/form-data" id="nvPd_image" class="columninput">
					<input type="text" id="pB_g" name="pB_G" value="<?php echo $idR ?>" required style="display:none;" />
					<label>*Nombre precio</label>
					<input type="text" id="Gga" required />
					<label>*Precio Normal (000.00)</label>
					<input type="number" id="Ggb" required />
					<label>*Precio Mayor (000.00)</label>
					<input type="number" id="Ggc" required />
					<label>*Cantidad Normal</label>
					<input type="number" id="Ggd" required />
					<label>*Cantidad Mayor</label>
					<input type="number" id="Gge" required />
					<div id="txQ"></div>
					<input type="submit" value="Ingresar" id="nvrlA" class="botonstyle" />
				</form>
			</article>
			<article class="flexjustyB">
			<?php
				error_reporting(E_ALL ^ E_NOTICE);
				$tamno_pagina=15;
				$pagina= $_GET['pagina'];
				if (!$pagina) {
					$inicio=0;
					$pagina=1;
				}
				else{
					$inicio= ($pagina - 1)*$tamno_pagina;
				}
				$ssql="SELECT * from preccant where p_id=$idR order by id_pcct desc";
				$rs=$conexion->query($ssql) or die (mysqli_error());
				$num_total_registros= $rs->num_rows;
				$total_paginas= ceil($num_total_registros / $tamno_pagina);
				$gsql="SELECT * from preccant where p_id=$idR order by id_pcct desc limit $inicio, $tamno_pagina";
				$impsql=$conexion->query($gsql) or die (mysqli_error());
				while ($gh=$impsql->fetch_assoc()) {
					$idJ=$gh['id_pcct'];
					$ppJ=$gh['p_id'];
					$nmJ=$gh['nm_pcct'];
					$p1j=$gh['prc1'];
					$p2j=$gh['prc2'];
					$c1j=$gh['cat1'];
					$c2j=$gh['cat2'];
			?>
			<article class="columninput">
				<label>*Nombre</label>
				<input type="text" id="Gga_<?php echo $idJ ?>" value="<?php echo $nmJ ?>" required />
				<label>*Precio Normal</label>
				<input type="number" id="Ggb_<?php echo $idJ ?>" value="<?php echo $p1j ?>" required />
				<label>*Precio Mayor</label>
				<input type="number" id="Ggc_<?php echo $idJ ?>" value="<?php echo $p2j ?>" required />
				<label>*Cantidad Normal</label>
				<input type="number" id="Ggd_<?php echo $idJ ?>" value="<?php echo $c1j ?>" required />
				<label>*Cantidad Mayor</label>
				<input type="number" id="Gge_<?php echo $idJ ?>" value="<?php echo $c2j ?>" required />
				<div id="txQ_<?php echo $idJ ?>"></div>
				<input type="submit" value="Modificar" class="carpcct" class="botonstyle" data-pp="<?php echo $idR ?>" data-id="<?php echo $idJ ?>" />
				<a class="dell" href="borr_rela1.php?br=<?php echo $idJ ?>&cr=<?php echo $idR ?>">Borrar</a>
			</article>
			<?php
				}
			?>
		</article>
		<article>
			<br />
			<b>Páginas: </b>
			<?php
			//muestro los distintos indices de las paginas
			if ($total_paginas>1) {
				for ($i=1; $i <=$total_paginas ; $i++) { 
					if ($pagina==$i) {
						//si muestro el indice del la pagina actual, no coloco enlace
			?>
				<b><?php echo $pagina." "; ?></b>
			<?php
					}
					else{
						//si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página 
			?>
						<a href="pr_rel.php?pd=<?php echo $idR ?>&pagina=<?php echo $i ?>"><?php echo "$i"; ?></a>

			<?php
					}
				}
			}
			?>
		</article>
		</article>
	</section>
	<footer>
		<article id="redes" class="auclasmarg">
			<a href="https://www.facebook.com/pages/Bodega-Los-Angeles-Inc/515911208524753?fref=ts" target="_blank"><span class="icon-facebook4"></span></a>
			<a href="" target="_blank"><span class="icon-twitter4"></span></a>
			<a href="https://instagram.com/losangelesinc/" target="_blank"><span class="icon-instagram2"></span></a>
			<a href="https://www.youtube.com/results?search_query=bodega+los+angelesinc" target="_blank"><span class="icon-youtube5"></span></a>
		</article>
		<article id="automargen" class="footeflx">
			<article class="columart">
				<a href="../imagenes">Imágenes</a>
				<a href="../productos">Productos</a>
				<a href="../videos">Videos</a>
				<a href="../eventos">Eventos</a>
				<a href="../factura">Historial Ventas</a>
			</article>
			<article class="columart">
				<a href="../usuarios">Usuarios</a>
				<a href="../sectores">Sectores</a>
				<a href="../donde">Donde Estamos</a>
				<a href="../../../cerrar">Salir</a>
			</article> 
		</article>
		<article class="fooffin">
			CONAXPORT © 2015 &nbsp;&nbsp;todos los derechos reservados &nbsp;- &nbsp;PBX (5) 841 733 &nbsp;&nbsp;Cúcuta - Colombia &nbsp;&nbsp;
			<a href="http://conaxport.com/" target="_blank">www.conaxport.com</a>
		</article>
	</footer>
</body>
</html>
<?php
		}
	}
	else{
		echo "<script type='text/javascript'>";
			echo "var pagina='../../erroradmin.html';";
			echo "document.location.href=pagina;";
		echo "</script>";
	}
?>