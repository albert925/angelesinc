<?php
	session_start();
	include '../../../config.php';
	if (isset($_SESSION['adm'])) {
		$usrRad=$_SESSION['adm'];
		$datos_adm="SELECT * from administrador where useradm='$usrRad'";
		$sql_datad=$conexion->query($datos_adm) or die (mysqli_error());
		while ($ad=$sql_datad->fetch_assoc()) {
			$idad=$ad['id_adm'];
			$usad=$ad['useradm'];
			$corad=$ad['cor_adm'];
		}
		$idR=$_GET['PD'];
		if ($idR=="") {
			echo "<script type='text/javascript'>";
				echo "alert('Id producto no disponible');";
				echo "var pagina='../productos';";
				echo "document.location.href=pagina;";
			echo "</script>";
		}
		else{
			$datos_PD="SELECT * from producto where cod_p=$idR";
			$sql_pdR=$conexion->query($datos_PD) or die (mysqli_error());
			while ($dtp=$sql_pdR->fetch_assoc()) {
				$iD=$dtp['cod_p'];
				$nmD=$dtp['nam_p'];
				$clidD=$dtp['cl_id'];
				$tpD=$dtp['tip_id'];
				$mkD=$dtp['mark_id'];
				$pcaD=$dtp['precA_p'];
				$pcnD=$dtp['precN_p'];
				$pctD=$dtp['precT_p'];
				$pcddD=$dtp['prec4'];
				$pceeD=$dtp['prec5'];
				$pcffD=$dtp['prec6'];
				$pcggD=$dtp['prec7'];
				$pchhD=$dtp['prec8'];
				$pciiD=$dtp['prec9'];
				$pcjjD=$dtp['prec10'];
				$pckkD=$dtp['prec11'];
				$pcllD=$dtp['prec12'];
				$pcmmD=$dtp['prec13'];
				$pcnnD=$dtp['prec14'];
				$pcooD=$dtp['prec15'];
				$pcppD=$dtp['prec16'];
				$pcqqD=$dtp['prec17'];
				$ctD=$dtp['cant_p'];
				$txD=$dtp['txt_p'];
			}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, maximun-scale=1" />
	<title>Angeles Inc</title>
	<link rel="icon" href="../../../imagenes/icono.png" />
	<link rel="stylesheet" href="../../../css/normalize.css" />
	<link rel="stylesheet" href="../../../css/iconos/style.css" />
	<link rel="stylesheet" href="../../../css/style.css" />
	<link rel="stylesheet" href="../../../css/styleadmin.css" />
	<script src="../../../js/jquery_2_1_1.js"></script>
	<script src="../../../js/scripadmin.js"></script>
	<script src="../../../ckeditor/ckeditor.js"></script>
	<script src="../../../js/productos.js"></script>
</head>
<body>
	<header>
		<figure id="logo">
			<a href="../">
				<img src="../../../imagenes/2.png" alt="logo" />
			</a>
		</figure>
	</header>
	<article id="mnuPp">
		<nav id="mnP">
			<ul>
				<li class="submen" data-num="3"><a href="../imagenes">Imágenes</a>
					<ul class="children3">
						<li><a href="../markfoot">Marcas I.</a></li>
					</ul>
				</li>
				<li><a class="selC" href="../productos">Productos</a></li>
				<li class="submen" data-num="1">
					<a href="../videos">Videos</a>
				</li>
				<li><a href="../eventos">Eventos</a></li>
				<li class="submen" data-num="2"><a href="../factura">Historial Ventas</a>
				</li>
				<li><a href="../usuarios">Usuarios</a></li>
				<li><a href="../sectores">Sectores</a></li>
				<li><a href="../donde">Donde Estamos</a></li>
				<li><a href="../../../cerrar">Salir</a></li>
			</ul>
		</nav>
		<div id="mnmov"><span class="icon-menu"></span></div>
	</article>
	<nav id="mnA">
		<a href="../"><?php echo "$usrRad"; ?></a>
		<a href="../productos">Ver Productos</a>
		<a href="tipo_P.php">Tipos de Producto</a>
		<a href="clase_p.php">Colecciones</a>
		<a href="mark_p.php">Marcas de producto</a>
		<a href="img_p.php">Nueva Imagen Producto</a>
	</nav>
	<section>
		<h1><?php echo "$nmD"; ?></h1>
		<article id="automargen">
			<article>
				<form action="modif_produ.php" method="post" class="columninput">
					<input type="text" name="idDc" value="<?php echo $idR ?>" required="required" style="display:none;" />
					<label><b>Nombre</b></label>
					<input type="text" name="nmP" value="<?php echo $nmD ?>" required="required" />
					<label><b>Tipo</b></label>
					<select id="StpF" name="Stp">
						<?php
							$tpV="SELECT * from cliente order by nam_cl asc";
							$sql_tpV=$conexion->query($tpV) or die (mysqli_error());
							while ($tP=$sql_tpV->fetch_assoc()) {
								$idTP=$tP['id_cl'];
								$nmTP=$tP['nam_cl'];
								if ($idTP==$clidD) {
									$seluno="selected";
								}
								else{
									$seluno="";
								}
						?>
						<option value="<?php echo $idTP ?>" <?php echo $seluno ?>><?php echo "$nmTP"; ?></option>
						<?php
							}
						?>
					</select>
					<label><b>Clase</b></label>
					<select id="SclF" name="Scl">
						<?php
							$clV="SELECT * from tip_producto order by nam_tp asc";
							$sql_clV=$conexion->query($clV) or die (mysqli_error());
							while ($tC=$sql_clV->fetch_assoc()) {
								$idCL=$tC['id_tp'];
								$nmCL=$tC['nam_tp'];
								if ($idCL==$tpD) {
									$seldos="selected";
								}
								else{
									$seldos="";
								}
						?>
						<option value="<?php echo $idCL ?>" <?php echo $seldos ?>><?php echo "$nmCL"; ?></option>
						<?php
							}
						?>
					</select>
					<label><b>Marca</b></label>
					<select id="SmkF" name="Smk">
						<option value="0">Marcas</option>
						<?php
							$mkV="SELECT * from marca order by nam_mk asc";
							$sql_mkV=$conexion->query($mkV) or die (mysqli_error());
							while ($tK=$sql_mkV->fetch_assoc()) {
								$idMK=$tK['id_mk'];
								$nmMK=$tK['nam_mk'];
								if ($idMK==$mkD) {
									$seltres="selected";
								}
								else{
									$seltres="";
								}
						?>
						<option value="<?php echo $idMK ?>" <?php echo $seltres ?>><?php echo "$nmMK"; ?></option>
						<?php
							}
						?>
					</select>
					<label><b>Precios</b></label>
					<article id="prciosflx">
						<input type="number" name="pcA" value="<?php echo $pcaD ?>" placeholder="Blusa" required />
						<input type="number" name="pcN" value="<?php echo $pcnD ?>" placeholder="Bluson" required />
						<input type="number" name="pcT" value="<?php echo $pctD ?>" placeholder="Chaqueta" required />
						<input type="number" name="pcdd" value="<?php echo $pcddD ?>" placeholder="Buso" required />
						<input type="number" name="pcee" value="<?php echo $pceeD ?>" placeholder="Top-sup" required />
					</article>
					<article id="prciosflx">
						<input type="number" name="pcff" value="<?php echo $pcffD ?>" placeholder="Top-inf" required />
						<input type="number" name="pcgg" value="<?php echo $pcggD ?>" placeholder="Short" required />
						<input type="number" name="pchh" value="<?php echo $pchhD ?>" placeholder="Falda" required />
						<input type="number" name="pcii" value="<?php echo $pciiD ?>" placeholder="Pantalón" required />
						<input type="number" name="pcjj" value="<?php echo $pcjjD ?>" placeholder="Vestido" required />
					</article>
					<article id="prciosflx">
						<input type="number" name="pckk" value="<?php echo $pckkD ?>" placeholder="Bolso" required />
						<input type="number" name="pcll" value="<?php echo $pcllD ?>" placeholder="Anillo" required />
						<input type="number" name="pcmm" value="<?php echo $pcmmD ?>" placeholder="Arete" required />
						<input type="number" name="pcnn" value="<?php echo $pcnnD ?>" placeholder="Candonga" required />
						<input type="number" name="pcoo" value="<?php echo $pcooD ?>" placeholder="Pulsera" required />
					</article>
					<article id="prciosflx">
						<input type="number" name="pcpp" value="<?php echo $pcppD ?>" placeholder="Collares" required />
						<input type="number" name="pcqq" value="0" placeholder="17" required style="display:none;" />
					</article>
					<label><b>Cantidad</b></label>
					<input type="number" name="cant" value="<?php echo $ctD ?>" required="required" />
					<label><b>Descripción</b></label>
					<textarea name="desPF" id="editor1"><?php echo "$txD"; ?></textarea>
					<script>
						CKEDITOR.replace('desPF');
					</script>
					<div id="msPP"></div>
					<input type="submit" value="Modificar" class="botonstyle" />
				</form>
			</article>
		</article>
	</section>
	<footer>
		<article id="redes" class="auclasmarg">
			<a href="https://www.facebook.com/pages/Bodega-Los-Angeles-Inc/515911208524753?fref=ts" target="_blank"><span class="icon-facebook4"></span></a>
			<a href="" target="_blank"><span class="icon-twitter4"></span></a>
			<a href="https://instagram.com/losangelesinc/" target="_blank"><span class="icon-instagram2"></span></a>
			<a href="https://www.youtube.com/results?search_query=bodega+los+angelesinc" target="_blank"><span class="icon-youtube5"></span></a>
		</article>
		<article id="automargen" class="footeflx">
			<article class="columart">
				<a href="../imagenes">Imágenes</a>
				<a href="../productos">Productos</a>
				<a href="../videos">Videos</a>
				<a href="../eventos">Eventos</a>
				<a href="../factura">Historial Ventas</a>
			</article>
			<article class="columart">
				<a href="../usuarios">Usuarios</a>
				<a href="../sectores">Sectores</a>
				<a href="../donde">Donde Estamos</a>
				<a href="../../../cerrar">Salir</a>
			</article> 
		</article>
		<article class="fooffin">
			CONAXPORT © 2015 &nbsp;&nbsp;todos los derechos reservados &nbsp;- &nbsp;PBX (5) 841 733 &nbsp;&nbsp;Cúcuta - Colombia &nbsp;&nbsp;
			<a href="http://conaxport.com/" target="_blank">www.conaxport.com</a>
		</article>
	</footer>
</body>
</html>
<?php
		}
	}
	else{
		echo "<script type='text/javascript'>";
			echo "var pagina='../../erroradmin.html';";
			echo "document.location.href=pagina;";
		echo "</script>";
	}
?>